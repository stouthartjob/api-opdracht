<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Models\User;
use App\Models\Posts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PostsController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api');

    }

    //posts a new post with variable: body
    public function post(Request $request)
    {
        $validated = Validator::make($request->all(), ['body' => 'required',]);

        if ($validated->fails()) {
            return response()->json($validated->errors(), 400);
        }

        $post = Posts::create([
            'user_id' => auth()->id(),
            'body' => $request->body,
        ]);

        return response()->json([
            'message' => 'Post send',
            'post' => $post
        ], 200);
    }


    //lists all posts
    public function posts()
    {
        $posts = Posts::get();

        return response()->json($posts, 200);
    }

    //shows single post using paramater.
    //example: /localhost:8000/api/posts/1
    public function single_post(Request $request)
    {
        $post = Posts::find($request->id);

        return response()->json($post, 200);
    }
}
