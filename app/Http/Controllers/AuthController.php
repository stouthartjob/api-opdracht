<?php

namespace App\Http\Controllers;

use Auth;
use Validator;
use App\Models\User;
use App\Models\Posts;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class AuthController extends Controller
{
    public function __construct()
    {
        auth()->setDefaultDriver('api');
        $this->middleware('auth:api', ['except' => ['login', 'register']]);

    }

    //Registers a user with variables:
    // Name
    // Username
    // Email
    // password, password has to be confirmed and is hashed.
    public function register(Request $request)
    {
        $validated = Validator::make($request->all(), [
            'name' => 'required|max:255',
            'username' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed',
        ]);

        if($validated->fails()) {
            return response()->json($validated->errors(), 400);
        }

        $user = User::create([
            'name' => $request->name,
            'username' => $request->username,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        return response()->json([
                'message' => 'User successfully registered',
                'user' => $user
        ], 200);
    }

    //logs a user in with variables:
    // email
    // pasword
    public function login(Request $request)
    {
        $validated = Validator::make($request->all(), [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        if ($validated->fails()) {
            return response()->json($validated->errors(), 422);
        }

        $token = auth()->attempt($validated->validated());
        return response()->json(['token' => $token], 200);
    }

    //logs a user out
    public function logout()
    {
        auth()->logout();

        return response()->json(['message' => 'User logged out'], 200);
    }
}
